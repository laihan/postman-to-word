package com.zhexiao.convert.entity.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zhe.xiao
 * @date 2021-02-06 10:27
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class CellContent {
    private String content;
    private RunStyle runStyle = new RunStyle();
}
