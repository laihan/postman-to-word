package com.zhexiao.convert.entity.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhe.xiao
 * @date 2020/09/17
 * @description
 */
@Data
@Accessors(chain = true)
public class ConvertDTO {
    /**
     * 调用系统
     */
    private String callSystem;

    /**
     * 数据格式
     */
    private String dataFormat;

    /**
     * 返回值
     */
    private String returns;
}
